<!DOCTYPE html>
<html>
  <head>
    <title>Mutt 2.2 Release Notes</title>
    <meta charset="utf-8"/>
  </head>

  <body>
    <h1>Mutt 2.2 Release Notes</h1>
    <p>
      Note: this is a review of some of the more interesting features
      in this release.  For a complete list of changes please be sure
      to read the
      <a href="https://gitlab.com/muttmua/mutt/raw/stable/UPDATING">UPDATING</a>
      file.
    </p>


    <h2>Maintainer Update</h2>
    <p>
      This obviously isn't a feature, but I wanted to mention that I
      will be moving away from Mutt maintainership after this release.
      There isn't a transition plan, so I'll keep maintaining the
      2.2.x series with bug fixes and security issues.
    </p>
    <p>
      It's been my pleasure to keep the releases coming since version
      1.5.24.  Unfortunately the past year, my time and energy
      available has been decreasing.  So my plan is to focus the time
      I do have on keeping Mutt stable, secure, and bug free; until
      someone else has the desire to head up (and support) new-feature
      releases.  Thank you everyone!
    </p>


    <h2>Thread Sorting</h2>
    <p>
      Setting <a href="/doc/manual/#sort">$sort</a> to
      <code class="literal">&quot;threads&quot;</code> enables threaded
      viewing in the index.  Previously, only
      <a href="/doc/manual/#sort-aux">$sort_aux</a> was used to affect sort order
      between and within threads.
    </p>

    <p>
      This release adds
      <a href="/doc/manual/#sort-thread-groups">$sort_thread_groups</a>,
      which controls top-level sorting between threads.  For backward
      compatibility, it defaults to
      <code class="literal">&quot;aux&quot;</code>, which delegates to
      $sort_aux.  However, it can be set to any value completely
      separate from $sort_aux.
    </p>

    <p>
      For example, to display the most recently active threads at the
      <b>top</b> of the index, while conversations within the thread
      (i.e. subthreads) remain sorted in normal order from top to
      bottom:
    </p>
    <pre>
      set sort               = threads
      set sort_thread_groups = reverse-last-date
      set sort_aux           = date
    </pre>

    <p>
      Note that both $sort_thread_groups and $sort_aux can use the
      &quot;reverse-&quot; and &quot;last-&quot; prefixes, and that
      their sort keys can be completely differently:
    </p>
    <pre>
      set sort               = threads
      set sort_thread_groups = reverse-subject
      set sort_aux           = last-date
    </pre>

    <p>
      However, using the &quot;last-&quot; prefix in both
      $sort_thread_groups and $sort_aux, with <b>different</b> sort
      keys can slow threading down.  To handle that case, Mutt needs
      to perform two scans for the &quot;last&quot; value: one for
      each key.
    </p>


    <h2>Separate Enter, Return, and KeypadEnter Bindings</h2>
    <p>
      This has been requested several times, and there was even a
      patch in the old Trac system for this feature.  I resisted, but
      Vincent Lefèvre finally helped persuade me in
      <a href="https://gitlab.com/muttmua/mutt/-/issues/362">ticket #362</a>.
    </p>
    <p>
      The patch allows the <code class="literal">&lt;Enter&gt;</code> and
      <code class="literal">&lt;Return&gt;</code> keys to be bound
      separately.  It also creates a new key name
      <code class="literal">&lt;KeypadEnter&gt;</code> reflecting the
      enter key on the number keypad (this requires a version of
      NCurses supporting the key).  Perhaps more practically, this allows
      <code class="literal">ctrl-j</code>
      and <code class="literal">ctrl-m</code> to also be bound
      separately.
    </p>

    <p>
      See <a href="/doc/manual/#crlf">Enter versus Return</a> in the
      <a href="/doc/manual/#bind">Changing the Default Key
        Bindings</a> section of the manual for details about the keys
      and how they relate to the <code class="literal">ctrl-j</code>
      and
      <code class="literal">ctrl-m</code> keys in terminal
      applications.  If desired, also see the commits
      <a href="https://gitlab.com/muttmua/mutt/-/commit/f4ff768c2764502a974042e1fe9d861a7a92fbcf">f4ff768c</a> and
      <a href="https://gitlab.com/muttmua/mutt/-/commit/4021ab59f021abd84742ae36895158c7129cbb38">4021ab59</a>.
    </p>


    <h2>GNU SASL support</h2>
    <p>
      Due to a potential license conflict, I've added support for using
      <a href="https://www.gnu.org/software/gsasl/">GNU SASL</a> as Mutt's
      SASL implementation library.  This can be specified via the
      <code class="literal">--with-gsasl</code> compile-time
      configuration option.
    </p>
    <p>
      There are very likely some bugs and issues to be ironed out, so please
      report issues you find.
    </p>
    <p>
      For more details see
      <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=999672"
         >Debian ticket 999672</a>.
    </p>


    <h2>Quadoption and Boolean Prompts Help</h2>
    <p>
      Some of the yes/no prompts in Mutt are controlled by
      quadoption or boolean configuration variables.  While it's convenient
      to be able to change the prompt behavior (by turning it off, or by
      setting the default answer), it can be difficult sometimes to find the
      right configuration variable to set.
    </p>

    <p>
      Starting this release, these prompts will have an additional &quot;?&quot;
      listed.  For example the prompt when exiting Mutt:<br>
      <img src="prompt1.png">
    </p>

    <p>
      Typing &quot;?&quot; will display the configuration option used
      to control the prompt:<br>
      <img src="prompt2.png">
    </p>

    <h2>Preserving context with &lt;skip-quoted&gt;</h2>
    <p>
      By default,
      using <code class="literal">&lt;skip-quoted&gt;</code> in the
      pager (by default bound
      to <code class="literal">&quot;S&quot;</code>) will skip
      directly to the top of the next unquoted part of the email.
      This can be useful to quickly jump over a quoted section you've
      read before, but can also be a bit disorienting without context.
    </p>
    <p>
      <a href="/doc/manual/#pager-skip-quoted-context"
         >$pager_skip_quoted_context</a> sets the number of quoted
      lines to display above the unquoted section.
    </p>
    <p>
      I was initially skeptical, but after trying it out I think it
      greatly increases the usability of
      <code class="literal">&lt;skip-quoted&gt;</code>.  Please try it
      out, and see what you think!
    </p>

    <h2>Converting text attachments when saving</h2>
    <p>
      The <a href="/doc/manual/#attach-save-charset-convert"
             >$attach_save_charset_convert</a> quadoption (defaulting
      to 
      <code class="literal">&quot;ask-yes&quot;</code>)
      will convert a text-type attachment's character set when saving it
      from the attachment menu.
    </p>


    <h2>$rfc2047_parameters enabled by default</h2>
    <p>
      If you've ever looked at the raw headers of an email and seen
      text like
      <code class="literal">=?iso-8859-1?q?blah=20blah=20blah?=</code>,
      that is RFC2047 encoding.  It is used to encode non-ascii text
      in an email header, but has specific rules about where it can be
      used.
    </p>
    <p>
      One of the places it <b>shouldn't</b> be used is for attachment
      names (usually stored
      inside <code class="literal">Content-Disposition:</code>,
      although historically it could be in other places.)  RFC2231
      encoding was created for that purpose.
    </p>
    <p>
      Unfortunately, even though it's
      <a href="https://datatracker.ietf.org/doc/html/rfc2047#page-8">explicitly
        listed as prohibited</a> by the RFC, a few mail clients still
      do so.  You've seen this if you tried to save an attachment
      and saw a name like the one above.
    </p>
    <p>
      Mutt has
      had <a href="/doc/manual/#rfc2047-parameters">$rfc2047_parameters</a>
      for a long time to deal with the problem, but it was set off by
      default, because.... the behavior was prohibited.  However, because
      the option name is fairly obsure, this often left users (even
      technically proficient ones) confused and blaming Mutt.  So
      starting this release, I've turned it on by default.
    </p>
    <p>
      I guess there may be a tiny chance someone wants to send an
      attachment
      named <code class="literal">=?iso-8859-1?q?blah=20blah=20blah?=</code>,
      but I think that's much less likely than normal users
      encountering the incorrect encoding.
    </p>
  </body>
</html>
