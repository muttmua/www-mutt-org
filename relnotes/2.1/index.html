<!DOCTYPE html>
<html>
  <head>
    <title>Mutt 2.1 Release Notes</title>
    <meta charset="utf-8"/>
  </head>

  <body>
    <h1>Mutt 2.1 Release Notes</h1>
    <p>
      Note: this is a review of some of the more interesting features
      in this release.  For a complete list of changes please be sure
      to read the
      <a href="https://gitlab.com/muttmua/mutt/raw/stable/UPDATING">UPDATING</a>
      file.
    </p>

    <h2>Build/infrastructure changes</h2>

    <h3>C99 libc required</h3>
    <p>
      Mutt has required a C99 compliant compiler since the 1.5.24
      release, but with this release it now also expects a C99
      compliant libc.  I've started pulling out checks, fixes, and
      internal replacements for functions that the C99 standard
      mandates.  This means you can't compile Mutt on an ancient
      system just by upgrading the compiler anymore.  (Or, if it
      happens to compile, expect it to work properly.)
    </p>

    <h3>gettext updated to 0.21</h3>
    <p>
      The gettext autoconf files have been updated to the latest
      release (0.21).  The installation was previously heavily
      customized, but those have been removed, resulting in a clean
      &quot;vanilla&quot; installation of gettext (which should be
      much easier to update in the future).  The latest gettext
      version no longer includes a bundled libintl, and so configure
      no longer supports the --with-included-gettext option.
    </p>

    <h3>autoconf 2.70 support</h3>
    <p>
      The autoconf files have been updated to support 2.70.  If you
      notice any issues, please let me know.
    </p>


    <h2>Viewing Attachments using copiousoutput mailcap entries</h2>
    <p>
      <code class="literal">&lt;view-pager&gt;</code> forces the use of a
      <code class="literal">copiousoutput</code> mailcap entry to view
      the attachment.  For example, let say you have a PDF attachment
      and a mailcap file with:
    </p>
    <pre>
      application/pdf; evince %s
      application/pdf; pdftotext -layout %s -; copiousoutput
    </pre>
    <p>
      Invoking <code class="literal">&lt;view-mailcap&gt;</code>
      (default 'm') will run evince.
      Invoking <code class="literal">&lt;view-attach&gt;</code>
      (default 'Enter') will also run evince, because application/pdf
      is not an internally supported MIME type.
      Invoking <code class="literal">&lt;view-text&gt;</code>
      (default 'T') will display the raw PDF code, which probably
      isn't useful to look at...
    </p>
    <p>
      The new function, <code class="literal">&lt;view-pager&gt;</code>,
      will cause Mutt to invoke pdftotext and view the output in the pager.
      For more details,
      including a discussion
      <code class="literal">&lt;view-attach&gt;</code> and internally
      supported MIME types, please see
      <a href="/doc/manual/#attach-menu"
      >Viewing Attachments</a> in the manual.
    </p>
    <p>
      An equivalent function <code class="literal">&lt;view-alt-pager&gt;</code>
      was added for previewing
      <a href="/doc/manual/#send-multipart-alternative-filter"
         >$send_multipart_alternative_filter</a> output using a
      <code class="literal">copiousoutput</code> mailcap entry.
    </p>
    <p>
      Also, the
      <code class="literal">&lt;view-mailcap&gt;</code>,
      <code class="literal">&lt;view-pager&gt;</code>, and
      <code class="literal">&lt;view-text&gt;</code> functions were
      all added to the compose menu, to allow viewing attachments
      in a variety of manner while composing too.
    </p>

    <h2>List Email Management Menu</h2>
    <p>
      <code class="literal">&lt;list-action&gt;</code>, bound to
      'Esc-L' by default, brings up a menu of actions to perform on a
      mailing list email:<br>
      <img src="list-action.png">
    </p>
    <p>
      The menu scans the currently selected email for common mailing
      list headers.  An entry with a mailto URL may be selected to
      begin composing an email to the address.
    </p>

    <h2>ANSI Color Improvements</h2>
    <p>
      Mutt now recognizes 256-color ANSI escape sequences, when
      <a href="/doc/manual/#allow-ansi">$allow_ansi</a> is set.
    </p>
    <p>
      If using a modern version of ncurses, Mutt will also use
      the extended color allocation API, enabling the simultaneous use of
      more than 256 color pairs at the same time (a limitation of the
      COLOR_PAIR() macro).
    </p>
    <p>
      These two new features allow the ability to view the output of
      256 color "image to ANSI text" generators inside Mutt:<br>
      <img src="color-pairs.png">
    </p>

    <h2>Message ID Format</h2>
    <p>
      The Mutt 2.0 release changed the default generated Message-ID
      format.  There are all sorts of opinions about how a Message-ID
      ought to be generated, though, so this release created a new
      configuration variable:
      <a href="/doc/manual/#message-id-format">$message_id_format</a>.
    </p>
    <p>
      The pre-2.0 Message-ID style can be generated by setting this to
      <code class="literal">&quot;&lt;%Y%02m%02d%02H%02M%02S.G%c%p@%f&gt;&quot;</code>,
      for instance.  There are a few other expandos that may be useful, but
      also note that format strings can use a
      <a href="/doc/manual/#formatstrings-filters">filter</a> and
      be generated by your own script.
    </p>
    <p>
      Mutt doesn't perform validation on the generated value; it's
      your responsibility to generate legal (and unique) values if you
      change the format string.  Please be a good netizen and
      customize responsibly.
    </p>

    <h2>Browser Menu Sorting</h2>
    <p>
      The browser menu operates in two modes: directory/file browsing, and
      mailbox browsing.  They can be toggled between using
      <code class="literal">&lt;toggle-mailboxes&gt;</code> (bound to
      'Tab' by default), or mailbox mode can be entered directly
      via <code class="literal">&lt;browse-mailboxes&gt;</code>.
    </p>
    <p>
      The modes previously shared the same sort variable,
      <a href="/doc/manual/#sort-browser">$sort_browser</a>, but this release
      creates a separate variable for mailbox mode:
      <a href="/doc/manual/#sort-browser-mailboxes">$sort_browser_mailboxes</a>.
    </p>
    <p>
      Manually changing the sort order via
      <code class="literal">&lt;sort&gt;</code> or
      <code class="literal">&lt;sort-reverse&gt;</code> will alter the active
      sort variable.
    </p>
    <p>
      The sort value &quot;unsorted&quot; now actually does sort: it
      does so in the order the values were added (for example the
      order listed in the muttrc).  So manually changing from
      &quot;unsorted&quot; to &quot;alpha&quot; and then back to
      &quot;unsorted&quot; again will now revert to the original
      order.
    </p>
    <p>
      In directory/file mode, the up-directory entry, &quot;..&quot;
      is now excluded from sorting and always listed at the top.
    </p>

    <h2>~h patterns over IMAP</h2>
    <p>
      Mutt will now download only the headers for each
      message, <b>unless</b> body caching is enabled.  (In that case,
      the whole message will be downloaded, but will be stored in the
      body cache.)  It's still not a great idea to use this pattern
      over IMAP, but hopefully this will make it a little bit less
      painful.
    </p>

    <h2>Monitored mailboxes without notification</h2>
    <p>
      Mutt 1.14 added the <code class="literal">-nopoll</code> option
      when specifying
      <a href="/doc/manual/#mailboxes">mailboxes</a>.  This allowed adding
      entries to the list that don't incur the penalty of polling, which
      could be useful for the sidebar.
    </p>
    <p>
      This release adds an inverse flag: <code class="literal">-nonotify</code>.
      This flag keeps the polling on, but stops Mutt from notifying when
      new mail is detected in the mailbox: either in the echo area, or via
      <a href="/doc/manual/#beep-new">$beep_new</a> or
      <a href="/doc/manual/#new-mail-command">$new_mail_command</a>.
    </p>
    <p>
      This could be useful if you want to show accurate message counts
      in the sidebar or mailbox browser for certain mailboxes, but don't
      want to be notified of changes in them.
    </p>

    <h2>GMT Date Header</h2>
    <p>
      When the new option
      <a href="/doc/manual/#local-date-header">$local_date_header</a>
      is unset, Mutt will generate Date headers using GMT, instead
      of the local timezone.
    </p>

    <h2>Pager Improvements</h2>
    <p>
      Some more performance improvements were made to the pager,
      affecting extremely long lines with multiple matching color
      regexps.  These aren't common, but if you ever opened a spam
      email filled with URLs and waited (and waited, and waited) for
      the pager to display, this should help some.
    </p>
    <p>
      While working on that, I cleaned up and made a few related pager
      fixes too.  Jumping to the middle of an email should handle
      coloring better in a few cases (e.g. for quoting, body patterns,
      and header colors.)  Searching in an email now also resolves
      color types and so should display headers properly.
    </p>
    <p>
      <code class="literal">&lt;skip-headers&gt;</code> bound to 'H'
      by default, will skip to the first blank line following the
      headers.
    </p>
    <p>
      A small change was made
      to <a href="/doc/manual/#hdr-order">hdr_order</a>, fixing it to
      find the longest match instead of the first match when ordering
      the headers.  In fact the example in the manual,
      &quot;<code class="literal">hdr_order From Date: From: To: Cc:
      Subject:</code>&quot; did not work properly before.  Both the
      From_ and From: header were previously displayed at the
      beginning.  With this change, the From: header will now be
      displayed after the Date: header.
    </p>

    <h2>Mailcap Filenames</h2>
    <p>
      This release cycle, a couple different users reported Mutt's
      mailcap file sanitization being a bit too strict.  Any 8-bit
      characters were converted to &quot;_&quot; which converted
      non-ascii filenames to "________".  This has been loosened a
      bit, hopefully producing more helpful filenames in those cases.
    </p>

  </body>
</html>
