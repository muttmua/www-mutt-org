<!DOCTYPE html>
<html>
  <head>
    <title>Mutt 1.12 Release Notes</title>
  </head>

  <body>
    <h1>Mutt 1.12 Release Notes</h1>
    <p>
      Note: this is a review of some of the more interesting features
      in this release.  For a complete list of changes please be sure
      to read the
      <a href="https://gitlab.com/muttmua/mutt/raw/stable/UPDATING">UPDATING</a>
      file.
    </p>

    <h2>Auto Subscribe</h2>
    <p>
      This is a feature originally written by Michael Elkins back in
      2010, but resurrected for my attention in a ticket.
    </p>
    <p>
      If you subscribe to a lot of mailing lists, and don't want to
      manually keep track of them
      via <a href="/doc/manual/#subscribe">subscribe</a>,
      <a href="/doc/manual/#auto-subscribe">$auto_subscribe</a> asks
      mutt to do it for you.  When mutt sees a List-Post header, it
      will add the mailing list to your subscribe list for you.
    </p>
    <p>
      Naturally, this slows down the reading of headers when opening a
      mailbox (I think that's the reason it was never merged).
      However, I've added a cache in front to keep things from slowing
      down too much.
    </p>

    <h2>Fcc after Sending</h2>
    <p>
      Previously, if sending failed, a copy of the message would still
      be in your Fcc mailbox.  This could result in multiple copies of
      a message that in actuality were never sent.  Starting this
      release, the Fcc will occur after sending the message succeeds.
    </p>
    <p>
      If the <b>fcc</b> fails, mutt will prompt to retry, or to save
      the message to another mailbox.
    </p>
    <p>
      In release 1.12.1,
      <a href="/doc/manual/#fcc-before-send">$fcc_before_send</a> was
      added to allow the previous behavior of Fcc'ing before sending.
      However, because of complications with the new Protected Headers
      code, the message cannot be modified before sending.  So
      <a href="/doc/manual/#fcc-attach">$fcc_attach</a> and
      <a href="/doc/manual/#fcc-clear">$fcc_clear</a> will not be
      respected when fcc'ing before sending.  If the fcc fails, mutt
      will prompt as described above.  If the send fails, then mutt
      will return to the compose menu, with the copy still remaining
      in the fcc mailbox.
    </p>

    <h2>Protected Headers</h2>
    <p>
      Protected Headers,
      aka <a href="https://github.com/autocrypt/memoryhole">Memory
      Hole</a> encompasses a range of functionality.  This release,
      Mutt is dipping its toes in, with basic support for the Subject
      header.
    </p>
    <p>
      <a href="/doc/manual/#crypt-protected-headers-read">$crypt_protected_headers_read</a>,
      default set, will read and display protected Subject headers.
      The protected Subject will be displayed inside the pager encryption/signing
      delimiters:<br/>
      <img src="prot-hdrs.png"></br> It will respect
      any <a href="/doc/manual/#color">color header</a> coloring, to
      help make it clear this is a header, not part of the message
      body.
    </p>
    <p>
      When
      <a href="/doc/manual/#crypt-protected-headers-write">$crypt_protected_headers_write</a>
      is set, a protected subject header will be generated for signed
      or encrypted emails.  For encrypted emails, the value in
      <a href="/doc/manual/#crypt-protected-headers-subject">$crypt_protected_headers_subject</a>
      will replace the real subject in the unencrypted headers.  You can also see this
      in the screenshot above: the envelope contains "Subject: Encrypted subject", while
      the encrypted protected header Subject is "Secret Project X".
    </p>
    <p>
      After opening a message, the protected subject will be
      stored in your header cache, to make it easier to search for
      when reopening the mailbox.  If
      <a href="/doc/manual/#crypt-protected-headers-save">$crypt_protected_headers_save</a>
      is set, the protected subject will also be stored back into the original
      email unencrypted headers.  Don't enable that unless you fully understand what
      it is doing and the security implications.
    </p>

    <h2>Color Enhancements</h2>
    <p>
      For a long time, Mutt has allowed prefixing color name with
      &quot;bright&quot; to add bold-faced, e.g. brightred.  Mutt now
      also allows the prefix &quot;light&quot;, which basically adds
      "8" to the color value behind the scenes.  In some terminals
      there is a large difference between the two.
    </p>
    <p>
      Attribute names are also now allowed in color lines.  They prefice
      the color:
    </p>
    <pre>
      set header_color_partial
      color header bold underline green default '^subject:'
    </pre>
    <p>
      <img src="bu-green-subject.png">
    </p>
    <p>
      (Note: the example screenshot is using
      <a href="/doc/manual/#header-color-partial">$header_color_partial</a>,
      added in 1.9.0.  Attribute names in the color lines does not
      depend on that being set, but I added it to the code snippet to make
      it clear the screenshot has that set too.)
    </p>

    <h2>Inline Forwarding of Attachments</h2>
    <p>
      Previously, if you wanted to include attachments in a forwarded
      email, you had to either
      use <a href="/doc/manual/#mime-forward">$mime_forward</a>, which
      forwarded the entire email as an attachment, or else open the
      attachment menu, manually tag all the attachments and then hit
      forward from the attachment menu.
    </p>
    <p>
      <a href="/doc/manual/#forward-attachments">$forward_attachments</a>
      is a quadoption that now allows forwarding attachments from an
      email when inline-forwarding (that
      is, <a href="/doc/manual/#mime-forward">$mime_forward</a> unset,
      and <a href="/doc/manual/#forward-decode">$forward_decode</a>
      set).  Note that text-decodable attachments will still be
      decoded and included in the message body.  Non-decodable
      attachments will be added as separate attachments.
    </p>

    <h2>TLS 1.0 and 1.1 Disabled by Default</h2>
    <p>
      This is not a &quot;feature&quot;, but after a couple people
      reported being affected by this, it seemed a good idea to
      mention it here.  In this release, TLS 1.0 and TLS 1.1 support
      is disabled by default.  They can be manually re-enabled via
      <a href="/doc/manual/#ssl-use-tlsv1">$ssl_use_tlsv1</a> and
      <a href="/doc/manual/#ssl-use-tlsv1-1">$ssl_use_tlsv1_1</a>.
    </p>
    <p>
      TLS 1.0 and 1.1 are deprecated and considered insecure, but some
      people on intranets, at the whim of vendors, may still be forced
      to use them.
    </p>

    <h2>Smaller Features</h2>

    <h3>&lt;descend-directory&gt;</h3>
    <p>
      This allows entering maildir and mh directories from the
      browser, which is useful if you maintain nested mailboxes.  It's
      not bound to a key by default.
    </p>

    <h3>&lt;group-chat-reply&gt;</h3>
    <p>
      The standard &lt;group-reply&gt; function doesn't preserve To vs
      Cc recipients in the reply.  The sender is placed in To, and all
      other recipients are in the Cc of the reply.  For many people
      this is just fine, and has been the default for a very long time.
    </p>
    <p>
      &lt;group-chat-reply&gt; preserves the distinction between
      recipients.  The sender <b>and To recipients</b> of the original
      email will be in the To of the reply.  This can be bound as you
      wish, to give an alternative or override the original function
      binding.
    </p>

    <h3>IMAP Header Downloading in Chunks</h3>
    <p>
      If you have a huge mailbox, it's possible for the IMAP server to
      timeout and close the connection just while downloading all the
      headers.  If you are having this problem, try setting
      <a href="/doc/manual/#imap-fetch-chunk-size">$imap_fetch_chunk_size</a>,
      perhaps to something like 10000 or 20000.  Mutt will then
      download the headers in batches of that size, sending a new
      FETCH command for each group.
    </p>
    <p>
      If you have a huge mailbox, it would be worth it to try enabling
      <a href="/doc/manual/#imap-qresync">$imap_qresync</a> in case
      your server supports it.  Also be sure to read the tips in
      <a href="/doc/manual/#tuning">Performance Tuning</a>.
    </p>

    <h3>Documentation Additions</h3>
    <p>
      A few new sections were added to the manual:
      <ul>
        <li>
          A <a href="/doc/manual/#quickconfig">starter muttrc</a>,
          with a link from the manual.
        </li>
        <li>
          A <a href="/doc/manual/#encryption">section on setting up encryption
            and signing</a>.
        </li>
        <li>
          An <a href="/doc/manual/#compose-flow">overview of message composition</a>
        </li>
        <li>
          A description
          of <a href="/doc/manual/#tab-thread-chars">thread status
          indicators</a>.
        </li>
      </ul>
    </p>

    <h2>Internal Improvements</h2>
    <p>
      A lot of internal improvements happened during this development
      cycle too:
      <ul>
        <li>
          Some degenerate pager coloring slowdowns were
          improved (usually generated by those huge spam messages.)
        </li>
        <li>
          RFC2047 decoding was made more robust against non-compliant emails.
        </li>
        <li>
          RFC2231 parameter continuations were implemented.
        </li>
        <li>
          Work continued on migrating away from fixed-size buffers to
          dynamic buffers.  There is still work to do, but this is
          improving support for really long filenames and paths, and
          helping to avoid arbitrary truncation of unexpectedly large
          values.
        </li>
        <li>
          Werner Koch contributed some cleanup and improvements to the
          GPGME code.  (Thanks, Werner!)
        </li>
      </ul>
    </p>
  </body>
</html>
